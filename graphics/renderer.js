class Renderer{
    constructor(){
        this.ambientLight = new Light();
        this.lights = [];
    }

    translateContext(x, y){
        translate(x, y)
    }

    rotateContext(angle){
        rotate(angle)
    }

    drawImage(sprite, x, y, w, h){
        //push();
        //translate(-w/2, -h/2);
        image(sprite, x, y, w, h);
        //pop();
        //stroke(255);
        //strokeWeight(4);
        //point(x, y);
    }

    drawText(text, x, y){
        text(text, x, y);
    }

    drawCircle(x, y, r){
        let d = r*2;
        ellipse(x, y, d, d);
    }

    drawRectangle(x, y, w, h){
        rect(x, y, w, h);
    }

    addLight(light){
        this.lights.push(light);
    }

    renderLight(){
        push();
        resetMatrix();
        
        loadPixels();
        for(let i = 0; i < pixels.length; i++){
            x = i / height;
            y = i / width;
                
            for(let light in lights){
                let d = abs(dist(light.x, light.y, x, y));
                if(d < light.strength){
                    
                }    
            }
        }
        updatePixels();
        
        pop();
    }

}