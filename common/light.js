class Light extends _Object{
    constructor(x, y, width, height, strength, color){
        super(x, y, width, height, 0, 0);
        this.strength = strength;
        this.color = color;
    }

}