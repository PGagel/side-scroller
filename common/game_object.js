class GameObject extends _Object{

    constructor(x, y, w, h, vx, vy, sprite, name){
        super(x, y, w, h, vx, vy);
        this.sprite = sprite;
        this.name = name;
    }

    show(renderer){
        renderer.drawImage(this.sprite, super.x(), super.y(), super.w(), super.h());
    }

    update(timeDelta, ax = 0, ay = 0){
        super.accelerate(ax, ay);
        super.move(timeDelta);
    }

}