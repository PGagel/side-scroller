class _Object{
    constructor(x, y, w, h, vx, vy){
        this.bb = new BoundingBox(x, y, w, h);
        this.vx = vx;
        this.vy = vy;
        this.ax = 0;
        this.ay = 0;
        this.propx = 0; 
        this.propy = 0;
        this.propel = false;
    }

    move(timeDelta){
        timeDelta /= 1000;

        this.bb.pos.x += this.vx * timeDelta;
        this.bb.pos.y += this.vy * timeDelta;

        this.vx += this.ax * timeDelta;
        this.vy += this.ay * timeDelta;
        
        if(this.propel){
            this.vx += this.propx * timeDelta;
            this.vy += this.propy * timeDelta;
        }

        this.ax = 0;
        this.ay = 0;
    }

    accelerate(ax, ay){
        this.ax += ax;
        this.ay += ay;
    }

    setPropulsion(on = true, propx = 0, propy = 0){
        this.propel = on
        if(on){
            this.propx = propx;
            this.propy = propy;
        }
    }

    x(){ return this.bb.pos.x; }

    y(){ return this.bb.pos.y; }

    w(){ return this.bb.w; }

    h(){ return this.bb.h; }

    /**
     * @param {Object} other Object to check collision with
     * @returns {Number} Number which defines the direction of collision
     */
    collision(other){
        return this.bb.collision(other.bb)
    }

    /**
     * @param {Object} other Object to check collision with
     * @returns {Boolean} true if collision happened or false if not
     */
    isColliding(other){
        return this.bb.isColliding(other.bb)
    }
}