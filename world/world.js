class World{
    constructor(speed = 1){
        this.blocks = [];
        this.progress = 0;
        this.before = millis();
        this.timeDelta = 0;
        this.speed = speed * 100;

        const size = 50;
        for(let i = 0; i < 100; i++){
            this.blocks.push(new GameObject(i*size, 300, size, size, -this.speed, 0, loadImage("images/red_square.png"), "block"+i));
        }
        
        this.blocks.push(new GameObject(30*size, 250, size, size, -this.speed, 0, loadImage("images/red_square.png"), "block"+"Zusatz"));
    }

    update(){
        let now = millis();
        this.progress++;
        this.timeDelta = now - this.before;

        this.blocks.forEach(block=>{
            block.update(this.timeDelta, 0, 0);
        });

        this.before = now;
    }

    fps(){
        return 1000/this.timeDelta;
    }

    show(renderer){
        this.blocks.forEach(block =>{
            block.show(renderer);
        });
    }

}