class Physics{
    
    constructor(world, g = 0, wind = createVector(0, 0), airResistance = 1){
        this.world = world;
        this.lastWorldState = this.world.progress;

        this.g = g * 100;
        this.wind = wind;
        this.airResistance = airResistance * 100;
    }

    update(objs){
        let currentWorldState = this.world.progress;

        if(this.lastWorldState == currentWorldState) return;

        let timeDelta = this.world.timeDelta;

        // TODO: Manufacture the collision detection to use Quad-Tree
        objs.forEach(obj => {
            objs.forEach(other => {
                if(obj !== other){
                    let dir = obj.collision(other);
                    switch(dir){
                        case Direction.left:
                        case Direction.right:
                            obj.vx = 0;
                            break;
                        case Direction.up:
                        case Direction.down:
                            obj.vy = 0;
                            break;
                        default:
                    }
                }
            });

            world.blocks.forEach(block => {
                let collision = obj.collision(block);
                switch(collision.direction){
                    case Direction.left:
                        obj.bb.pos.x += collision.overlap;
                        obj.vx = block.vx;
                        break;
                    case Direction.right:
                        obj.bb.pos.x -= collision.overlap;
                        obj.vx = block.vx;
                        break;
                    case Direction.up:
                        obj.bb.pos.y += collision.overlap;
                        obj.vy = block.vy;
                        break;
                    case Direction.down:
                        obj.bb.pos.y -= collision.overlap;
                        obj.vy = block.vy;
                        break;
                    default:
                }
                
            });
        });
        
        let ax = this.wind.x;
        let ay = this.wind.y;

        ay += this.g;

        objs.forEach(obj => {

                if (obj.vx > 0) {
                    ax -= this.airResistance * obj.vx/100;
                } else if (obj.vx < 0) {
                    ax += this.airResistance * -obj.vx/100;
                }
              

                if (obj.vy > 0) {
                    ay -= this.airResistance * obj.vy/100;
                } else if (obj.vy < 0) {
                    ay += this.airResistance * -obj.vy/100;
                }
              

            if(obj.grounded) 
                obj.update(timeDelta, ax, 0);
            else
                obj.update(timeDelta, ax, ay);
        });

        this.lastWorldState = currentWorldState;
    }

}