const Direction = Object.freeze({"up" : 1, "down" : 2, "left" : 3, "right" : 4, "none":-1});

class BoundingBox{

    constructor(x, y, w, h){
        this.pos = createVector(x, y);
        this.w = w;
        this.h = h;
    }

    /**
     * @param {GameObject} other the object to check collision 
     * @returns {Number} Number which defines the direction of collision
     */
    collision(other){
        let direction = -1;
        let overlap = 0;
        if(this.isColliding(other)){
            let dirVec = p5.Vector.sub(this.pos, other.pos);
            let dirX = abs(dirVec.x);
            let dirY = abs(dirVec.y);

            if(dirX > dirY){
                
                if(dirVec.x < 0){
                    overlap = this.w - dirX;
                    direction = Direction.right;
                } else {
                    overlap = other.w - dirX;
                    direction = Direction.left;
                }
            } else {

                if(dirVec.y < 0){
                    overlap = this.h - dirY;
                    direction = Direction.down;
                } else {
                    overlap = other.h - dirY;
                    direction = Direction.up;
                }
            }
        }

        if (overlap < .2)  {
            direction = Direction.none;
            overlap = 0;
        }
        
        return {direction, overlap};
    }

    /**
     * @param {GameObject} other the object to check collision
     * @returns {Boolean} true if collision happened or false if not
     */
    isColliding(other){
        return !(other.pos.x > this.pos.x + this.w || other.pos.y > this.pos.y + this.h || this.pos.x > other.pos.x + other.w || this.pos.y > other.pos.y + other.h);
    }

}