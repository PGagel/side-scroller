
let handling = 20000;
let propulsionForce = 1000;

function handleKeyPressed(keyCode, key){
  switch(key){
    case "w":
      objects[0].bb.pos.y -= 1;
      objects[0].accelerate(0, -handling);
      break;
    case "s":
      objects[0].bb.pos.y += 1;
      objects[0].accelerate(0, handling);
      break;
    case "a":
      objects[0].setPropulsion(true, -propulsionForce, 0);
      break;
    case "d":
      objects[0].setPropulsion(true, propulsionForce, 0);
      break;
    case " ":
      console.log("PAUSE");
      break;
  }

  switch(keyCode){
    case UP_ARROW:
      objects[0].bb.pos.y -= 1;
      objects[0].accelerate(0, -handling);
      break;
    case DOWN_ARROW:
      objects[0].bb.pos.y += 1;
      objects[0].accelerate(0, handling);
      break;
    case LEFT_ARROW:
      objects[0].setPropulsion(true, -propulsionForce, 0);
      break;
    case RIGHT_ARROW:
      objects[0].setPropulsion(true, propulsionForce, 0);
      break;
    
  }
}

function handleKeyReleased(keyCode, key){
  switch(key){
    case "w":
      break;
    case "s":
      break;
    case "a":
      objects[0].setPropulsion(false);
      break;
    case "d":
      objects[0].setPropulsion(false);
      break;
  }

  switch(keyCode){
    case UP_ARROW:
      break;
    case DOWN_ARROW:
      break;
    case LEFT_ARROW:
      objects[0].setPropulsion(false);
      break;
    case RIGHT_ARROW:
      objects[0].setPropulsion(false);
      break;
    
  }
}