const objects = []
let world;
let renderer;
let physics;

function setup() {
  createCanvas(800, 600);
  
  world = new World(1);
  physics = new Physics(world, 9.81, createVector(0, 0), 2);
  renderer = new Renderer();
  
  square = loadImage("images/blue_square.png");
  let jumper = new GameObject(width/2, 0, 50, 50, 0, 0, square, "jumper");
  objects.push(jumper);
}

function updateScene() {
  physics.update(objects);
  world.update();
}

function draw() {
  // ===== UPDATE ===== //
  updateScene();
  
  // ===== DRAW ===== //
  background(51);
  
  world.show(renderer);

  objects.forEach(obj => {
    obj.show(renderer);
  });
}

function keyPressed() {
  handleKeyPressed(keyCode, key);
}

function keyReleased(){
  handleKeyReleased(keyCode, key);
}